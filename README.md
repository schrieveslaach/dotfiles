# dotfiles managed with [chezmoi](https://www.chezmoi.io/).

This repository contains the dotfiles to setup schrieveslaach's personal development environment
with [Neovim](https://neovim.io/), [Nushell](https://www.nushell.sh/) and
[WezTerm](https://wezfurlong.org/wezterm/).

## License

The content of this project itself is licensed under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/), and the underlying source code used to format and display that content is licensed under the [MIT license](LICENSE.md).

