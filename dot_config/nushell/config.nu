if (which ^carapace | length) == 0 {
   use $"($nu.home-path)/.config/nushell/completions/bat-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/cargo-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/curl-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/docker-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/git-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/man-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/npm-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/rustup-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/rg-completions.nu" *
   use $"($nu.home-path)/.config/nushell/completions/tar-completions.nu" *
}
use $"($nu.home-path)/.config/nushell/completions/zellij-completions.nu" *

use $"($nu.home-path)/.config/nushell/scripts/ffmpeg.nu" *
use $"($nu.home-path)/.config/nushell/scripts/pde.nu" *
use $"($nu.home-path)/.config/nushell/scripts/wezterm-helper.nu" *

use $"($nu.home-path)/.config/nushell/modules/system/mod.nu" clip

$env.EDITOR = "nvim"

echo paperwm status

def is-in-ssh-session [] {
   ($env | columns | filter {|c| $c in ["SSH_CLIENT", "SSH_CONNECTION"]} | length) > 0
}

def start_zellij [] {
   if (which ^zellij | length) == 0 or (is-in-ssh-session) {
      return
   }

   if 'ZELLIJ' not-in ($env | columns) {
      ^zellij
      exit
   }
}

start_zellij
