# Prints a Wezterm User Vars (see
# https://wezfurlong.org/wezterm/recipes/passing-data.html?h=set+user+var#user-vars)
# so that Wezterm can decide if the window title can go away
export def "echo paperwm status" [] {
   if (which ^gnome-extensions | length) == 0 {
      return
   }

   if not ((is-terminal --stdin) and (is-terminal --stderr) and (is-terminal --stdout)) and not (is-in-ssh-session) {
      return
   }

   let paperwm_info = (
      gnome-extensions info paperwm@paperwm.github.com
         | lines
         | skip
         | str trim
         | parse "{key}: {value}"
         | reduce -f {} {|it, acc| $acc | upsert $it.key $it.value }
   )

   let status = if ($paperwm_info | is-not-empty) and ($paperwm_info | get --ignore-errors Status | is-not-empty) {
      $paperwm_info.Status
   } else {
      "DISABLED"
   }

   ^echo -en $"\\033]1337;SetUserVar=paperwm-status=(echo $status | base64)\\007"
}

def is-in-ssh-session [] {
   ($env | columns | filter {|c| $c in ["SSH_CLIENT", "SSH_CONNECTION"]} | length) > 0
}
