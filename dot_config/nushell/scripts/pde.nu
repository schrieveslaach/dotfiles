# Queries all Docker containers for database containers (e.g. PostgreSQL)
export def db-containers [] {
   postgres-containers
}

def postgres-containers [] {
   let db_containers = (docker container ls --format '{{json .}}'
      | lines | each { |line| $line | from json }
      | where Image =~ "postgres"
      | each {|container| docker container inspect $container.ID | from json}
      | flatten
      | where NetworkSettings.Ports."5432/tcp" != null
   )

   # Map Env Variables to be namable
   let db_containers = ($db_containers
      | each {|c|
         let parsed_env = (
            $c.Config.Env
            | parse "{Key}={Value}"
            | flatten
            | reduce -f {} {|it, acc| $acc | upsert $it.Key $it.Value }
            | default "postgres" POSTGRES_USER
         )

         let parsed_env = (
            $parsed_env
            | default $parsed_env.POSTGRES_USER POSTGRES_DB
         )

         {
            name: ([($c.Name | str trim -l -c "/"), " - ", ($c.Id | str substring 0..6)] | str join),
            port: (($c.NetworkSettings.Ports."5432/tcp" | first).HostPort),
            user: $parsed_env.POSTGRES_USER,
            db: $parsed_env.POSTGRES_DB,
            pw: $parsed_env.POSTGRES_PASSWORD,
         }
      }
   )

   $db_containers
}

# Converts the output of db-containers into a table structure that is convertable into vim-dadbod-us dbs variable
export def "to dadbod-ui-dbs" [] {
   let db_containers = $in

   $db_containers | each {|c| {
      name: $c.name,
      url: (["postgres://", $c.user, ":", $c.pw, "@localhost:", $c.port, "/", $c.db] | str join)
   }}
}

# Lists all available JVMs
export def "jvms list" [] {
   try { hide-env JAVA_HOME }
   let jvm_properties = (
      (do { java -XshowSettings:properties -version } | complete)
      | get stderr
      | lines
      | str trim
      | where {|l| $l | str contains " = "}
      | parse "{prop} = {value}"
   )

   let jvm_base_dir = ($jvm_properties
      | where prop == "java.home"
      | get value
      | parse -r '^(?P<jdk_basedir>.+)\/(?P<jdk_dirname>[^\/]+\d+[^\/]+)(?P<jdk_homepart>\/.+)?$'
      | first
   )

   ls $jvm_base_dir.jdk_basedir
      | where name =~ '\d'
      | each {|jvm| {
         version: ($jvm.name | parse -r '(?P<version>\d+)' | get version | first | into int),
         home: ($jvm.name + $jvm_base_dir.jdk_homepart),
      }}
      | sort-by version
}

def "nu-complete jvm version numbers" [] {
   jvms list | each {|jvm| { value: $jvm.version, description: $jvm.home }}
}

# Defines JAVA_HOME from the list of available JVMs
export def --env "jvms set" [
   version: int@"nu-complete jvm version numbers"
] {
   $env.JAVA_HOME = (jvms list | where version == $version | get home | first)
}
