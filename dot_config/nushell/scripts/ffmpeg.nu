# Convert to give video file to GIF
export def convert2gif [
   path: path                   # Path to the video that should be converted
   output: path = "output.gif"  # Path to the GIF file that should be created
   --width (-w): int = 1280     # width of the GIF file
] {
   let palette_path = $"/tmp/palette-(random chars -l 16).png" 
   ffmpeg -y -i $"($path)" -vf fps=10,scale=1280:-1:flags=lanczos,palettegen $palette_path
   ffmpeg -i $"($path)" -i $palette_path -filter_complex $"fps=10,scale=($width):-1:flags=lanczos[x];[x][1:v]paletteuse" $"($output)"
   rm $palette_path
}

