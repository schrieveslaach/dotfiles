local wezterm = require("wezterm")
local mux = wezterm.mux

wezterm.on("gui-startup", function(cmd)
   local tab, pane, window = mux.spawn_window(cmd or {})
   window:gui_window():maximize()
end)

local config = wezterm.config_builder()

wezterm.on("user-var-changed", function(window, pane, name, value)
   wezterm.log_info("var", name, value)
   if name == "paperwm-status" then
      if value == "ACTIVE" then
         window:set_config_overrides({
            window_decorations = "NONE",
         })
      else
         window:set_config_overrides({
            window_decorations = "TITLE | RESIZE",
         })
      end

   end
end)

config.color_scheme = "nightfox"
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.font = wezterm.font({
   family = "Jetbrains Mono",
   weight = "Regular",
})
config.font_size = 9
config.hide_tab_bar_if_only_one_tab = true
config.freetype_load_target = "HorizontalLcd"

config.term = "wezterm"

local act = wezterm.action
config.keys = {
   { key = "F11", action = wezterm.action.ToggleFullScreen },
}
for i = 1, 9 do
   table.insert(config.keys, {
      key = tostring(i),
      mods = "ALT",
      action = act.ActivateTab(i - 1),
   })
end

config.background = {
   {
      source = {
         File = wezterm.config_dir .. "/background.gif",
      },
      width = "100%",
      repeat_x = "NoRepeat",
      vertical_align = "Middle",
   },
}

return config
