local M = {}

function M.home(version)
   local home = nil

   local job_id = vim.fn.jobstart({
      "nu",
      "--config",
      vim.env.HOME .. "/.config/nushell/config.nu",
      "-c",
      string.format("print (jvms list | where version == %d | get home | first) ; exit", version),
   }, {
      stdout_buffered = true,
      on_stdout = function(_, data)
         home = vim.trim(data[1])
      end,
   })

   vim.fn.jobwait({ job_id })

   return home
end

return M
