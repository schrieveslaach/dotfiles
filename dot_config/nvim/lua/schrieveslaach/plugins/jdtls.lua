return {
   {
      "mfussenegger/nvim-jdtls",
      ft = { "java" },
      config = function()
         local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
         local project_hash = string.sub(vim.api.nvim_call_function("sha256", { vim.fn.getcwd() }), 1, 6)
         local data_path = vim.env.HOME .. "/.cache/jdtls/" .. project_name .. "-" .. project_hash

         local on_attach = function(_, bufnr)
            require("jdtls.setup").add_commands()
            require("jdtls").setup_dap()
            require("schrieveslaach.lsp.keymaps").lsp_buf_set_keymap(bufnr)
         end

         require("jdtls").start_or_attach({
            on_attach = on_attach,
            cmd = {
               "jdtls",
               "-configuration",
               vim.env.HOME .. "/.cache/jdtls/configuration",
               "-data",
               data_path,
            },
            cmd_env = {
               JAVA_HOME = require("schrieveslaach.jvm").home(21),
            },
            init_options = {
               bundles = vim.split(vim.fn.glob(vim.env.MASON .. "/share/java-*/*.jar"), "\n"),
               extendedClientCapabilities = {
                  progressReportProvider = false,
               },
            },
            settings = {
               java = {
                  configuration = {
                     runtimes = {
                        {
                           name = "JavaSE-11",
                           path = require("schrieveslaach.jvm").home(11),
                        },
                        {
                           name = "JavaSE-17",
                           path = require("schrieveslaach.jvm").home(17),
                        },
                        {
                           name = "JavaSE-21",
                           path = require("schrieveslaach.jvm").home(21),
                        },
                     },
                  },
               },
            },
         })
      end,
   },
}
