return {
   {
      "luukvbaal/statuscol.nvim",
      dependencies = {
         "kevinhwang91/nvim-ufo",
         "kevinhwang91/promise-async",
      },
      config = function()
         local builtin = require("statuscol.builtin")
         require("statuscol").setup({
            segments = {
               {
                  text = { builtin.foldfunc, " " },
                  condition = { builtin.not_empty, true, builtin.not_empty },
                  click = "v:lua.ScFa",
               },
               { text = { "%s" }, click = "v:lua.ScSa" },
               { text = { builtin.lnumfunc, " " }, click = "v:lua.ScLa" },
            },
         })

         vim.opt.foldcolumn = "1"
         vim.opt.foldlevel = 99
         vim.opt.foldlevelstart = 99
         vim.opt.foldenable = true
         vim.opt.fillchars = [[eob: ,fold: ,foldopen:,foldsep:┃,foldclose:]]

         vim.keymap.set("n", "zR", require("ufo").openAllFolds)
         vim.keymap.set("n", "zM", require("ufo").closeAllFolds)

         require("ufo").setup({
            provider_selector = function()
               return { "treesitter", "indent" }
            end,
         })
      end,
   },
}
