return {
   {
      "tpope/vim-dadbod",
      dependencies = {
         "kristijanhusak/vim-dadbod-ui",
      },
      config = function()
         vim.api.nvim_create_user_command("DBUIAddContainerConnections", function()
            vim.fn.jobstart({
               "nu",
               "--config",
               vim.env.HOME .. "/.config/nushell/config.nu",
               "-c",
               "print (db-containers | to dadbod-ui-dbs | to json)",
            }, {
               stdout_buffered = true,
               on_stdout = function(_, data)
                  vim.g["dbs"] = vim.fn.json_decode(data)
               end,
            })
         end, {})
      end,
   },
}
