return {
   {
      "j-hui/fidget.nvim",
      config = function()
         require("fidget").setup({
            progress = {
               display = {
                  progress_icon = { pattern = "circle_quarters" },
               },
            },
            notification = {
               window = {
                  winblend = 0,
                  border = "rounded",
                  relative = "editor",
               },
            },
         })
      end,
   },
}
