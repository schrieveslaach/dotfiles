return {
   {
      "williamboman/mason.nvim",
      dependencies = {
         "williamboman/mason-lspconfig.nvim",
         "WhoIsSethDaniel/mason-tool-installer.nvim",
      },
      config = function()
         require("mason").setup()
         require("mason-lspconfig").setup()
         require("mason-tool-installer").setup({
            ensure_installed = {
               "clangd",
               "codelldb",
               "jedi-language-server",
               "kotlin_language_server",
               "lemminx",
               "lua_ls",
               "marksman",
               "sonarlint-language-server",
               "ts_ls",
               "volar",

               -- Java stuff
               "jdtls",
               "java-debug-adapter",
               "java-test",
            },
         })
      end,
   },
}
