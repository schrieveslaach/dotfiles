return {
   {
      "mfussenegger/nvim-dap",
      dependencies = {
         "nvim-neotest/nvim-nio",
         "rcarriga/nvim-dap-ui",
      },

      config = function()
         local dap, dapui = require("dap"), require("dapui")

         dapui.setup({
            layouts = {
               {
                  elements = {
                     { id = "scopes", size = 0.35 },
                     { id = "watches", size = 0.35 },
                     { id = "breakpoints", size = 0.3 },
                  },
                  size = 40,
                  position = "left",
               },
               {
                  elements = { "repl" },
                  size = 0.25,
                  position = "bottom",
               },
               {
                  elements = { "console" },
                  size = 0.25,
                  position = "bottom",
               },
            },
         })

         dap.listeners.after.event_initialized["dapui_config"] = function()
            dapui.open({ layout = 1, reset = true })
            dapui.open({ layout = 2, reset = true })

            for _, tab_id in ipairs(vim.api.nvim_list_tabpages()) do
               for _, win_id in ipairs(vim.api.nvim_tabpage_list_wins(tab_id)) do
                  local buf_id = vim.api.nvim_win_get_buf(win_id)
                  local filetype = vim.api.nvim_buf_get_option(buf_id, "filetype")
                  if filetype == "dapui_console" then
                     return
                  end
               end
            end

            dapui.open({ layout = 3, reset = true })

            local console_win_id = nil
            local console_buf_id = nil
            for _, win_id in ipairs(vim.api.nvim_list_wins()) do
               local buf_id = vim.api.nvim_win_get_buf(win_id)
               local filetype = vim.api.nvim_buf_get_option(buf_id, "filetype")
               if filetype == "dapui_console" then
                  console_win_id = win_id
                  console_buf_id = buf_id
               end
            end

            if console_win_id and console_buf_id then
               local current_tab = vim.api.nvim_get_current_tabpage()

               vim.cmd("tabnew")
               local new_tab_id = -1
               for _, tab_id in ipairs(vim.api.nvim_list_tabpages()) do
                  if tab_id > new_tab_id then
                     new_tab_id = tab_id
                  end
               end

               local win_id_in_new_tab = vim.api.nvim_tabpage_list_wins(new_tab_id)[1]
               vim.api.nvim_win_set_buf(win_id_in_new_tab, console_buf_id)
               vim.api.nvim_win_close(console_win_id, true)

               vim.api.nvim_set_current_tabpage(current_tab)
            end
         end
         dap.listeners.before.event_terminated["dapui_config"] = function() end
         dap.listeners.before.event_exited["dapui_config"] = function() end

         vim.keymap.set("n", "<leader>tt", function()
            dapui.toggle({ layout = 1, reset = true })
            dapui.toggle({ layout = 2, reset = true })
         end)
         vim.keymap.set("n", "<leader>tb", dap.toggle_breakpoint)
         vim.keymap.set("n", "<F7>", dap.step_into)
         vim.keymap.set("n", "<F8>", dap.step_over)
         vim.keymap.set("n", "<F9>", dap.continue)
         vim.keymap.set("n", "<leader>tf", function()
            if vim.api.nvim_buf_get_option(0, "filetype") == "java" then
               require("jdtls").test_class()
            end
         end)

         vim.keymap.set("n", "<leader>tm", function()
            if vim.api.nvim_buf_get_option(0, "filetype") == "java" then
               require("jdtls").test_nearest_method()
            end
         end)

         vim.api.nvim_create_user_command(
            "DebugRemoteProcess",
            require("schrieveslaach.dap.custom-actions").attach_to_remote_debugger,
            {}
         )
      end,
   },
}
