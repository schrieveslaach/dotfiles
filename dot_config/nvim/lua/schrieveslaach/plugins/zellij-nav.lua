return {
   {
      "https://git.sr.ht/~swaits/zellij-nav.nvim",
      config = function()
         require("zellij-nav").setup()

         vim.keymap.set("n", "<c-h>", "<cmd>ZellijNavigateLeft<cr>")
         vim.keymap.set("n", "<c-j>", "<cmd>ZellijNavigateDown<cr>")
         vim.keymap.set("n", "<c-k>", "<cmd>ZellijNavigateUp<cr>")
         vim.keymap.set("n", "<c-l>", "<cmd>ZellijNavigateRight<cr>")
      end,
   },
}
