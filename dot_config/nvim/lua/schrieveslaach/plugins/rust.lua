local active_rust_analyzer = function()
   local path = nil

   local job_id = vim.fn.jobstart({
      "nu",
      "--config",
      vim.env.HOME .. "/.config/nushell/config.nu",
      "-c",
      "print (ls $\"(rustup show home | str trim)/toolchains/(rustup show active-toolchain | parse '{chain} {rest}' | get chain | first)/bin/rust-analyzer\" | get name | first)",
   }, {
      stdout_buffered = true,
      on_stdout = function(_, data)
         if #data > 0 then
            path = vim.trim(data[1])
         end
      end,
   })

   vim.fn.jobwait({ job_id })

   if path == nil or string.len(path) == 0 then
      vim.notify(
         "Couldn't find rust-analyzer via rustup show active-toolchain. Please, make sure to add rust-analyzer via `rustup component add rust-analyzer`. Assuming that rust-analyzer is installed globally",
         vim.log.levels.WARN
      )
      return "rust-analyzer"
   end

   return path
end

return {
   {
      "mrcjkb/rustaceanvim",
      version = "^5",
      dependencies = {
         "hrsh7th/cmp-nvim-lsp",
      },
      ft = { "rust" },
      config = function()
         local capabilities = require("cmp_nvim_lsp").default_capabilities()

         vim.g.rustaceanvim = function()
            return {
               tools = {
                  enable_clippy = false,
               },
               server = {
                  cmd = { active_rust_analyzer() },
                  capabilities = capabilities,
                  on_attach = function(_, bufnr)
                     require("schrieveslaach.lsp.keymaps").lsp_buf_set_keymap(bufnr)
                  end,
               },
            }
         end
      end,
   },
}
