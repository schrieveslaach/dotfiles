return {
   {
      "numToStr/Comment.nvim",
      config = function()
         require("Comment").setup({
            toggler = {
               line = "χχ",
               block = "χβ",
            },

            opleader = {
               line = "χ",
               block = "β",
            },

            extra = {
               above = "χO",
               below = "χo",
               eol = "χA",
            },
         })
      end,
   },
}
