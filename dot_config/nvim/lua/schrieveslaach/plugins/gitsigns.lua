return {
   {
      "lewis6991/gitsigns.nvim",
      dependencies = {
         "nvim-lua/plenary.nvim",
      },
      config = function()
         require("gitsigns").setup({
            on_attach = function()
               vim.keymap.set("n", "<leader>gr", "<cmd>Gitsigns reset_hunk<cr>")
               vim.keymap.set("n", "<leader>gb", function()
                  require("gitsigns").blame_line({ full = true })
               end)

               vim.keymap.set("n", "]c", function()
                  if vim.wo.diff then
                     return "]c"
                  end
                  vim.schedule(function()
                     require("gitsigns").next_hunk()
                  end)
                  return "<Ignore>"
               end, { expr = true })

               vim.keymap.set("n", "[c", function()
                  if vim.wo.diff then
                     return "[c"
                  end
                  vim.schedule(function()
                     require("gitsigns").prev_hunk()
                  end)
                  return "<Ignore>"
               end, { expr = true })
            end,
         })
      end,
   },
}
