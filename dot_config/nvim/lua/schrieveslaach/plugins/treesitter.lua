return {
   {
      "nvim-treesitter/nvim-treesitter",
      cmd = "TSUpdate",
      dependencies = {
         "nvim-treesitter/nvim-treesitter-textobjects",
         "nvim-treesitter/playground",
      },
      config = function()
         require("nvim-treesitter.configs").setup({
            playground = {
               enabled = true,
            },
            indent = {
               enable = true,
            },
            highlight = {
               enable = true,
            },
            ensure_installed = {
               "bash",
               "c",
               "cpp",
               "css",
               "html",
               "java",
               "javascript",
               "json",
               "kotlin",
               "kdl",
               "lua",
               "markdown",
               "nu",
               "php",
               "rust",
               "toml",
               "vim",
               "vue",
               "yaml",
               "xml",
            },
            textobjects = {
               select = {
                  enable = true,
                  lookahead = true,
                  keymaps = {
                     ["ib"] = "@block.inner",
                     ["ic"] = "@class.inner",
                     ["if"] = "@function.inner",
                     ["il"] = "@loop.inner",
                  },
               },
            },
         })
      end,
   },
}
