local function choose_spell_checking()
   vim.ui.select({
      { code = "en_us", text = "English US" },
      { code = "de_20", text = "German" },
      { code = "none", text = "Disable spell check" },
   }, {
      prompt = "Select Spell Check",
      format_item = function(item)
         return item.text
      end,
   }, function(choice)
      if not choice then
         return
      end
      if choice.code == "none" then
         vim.opt.spelllang = {}
         vim.opt.spell = false
      else
         vim.opt.spelllang = { choice.code, "programming" }
         vim.opt.spell = true
      end
   end)
end

return {
   {
      "psliwka/vim-dirtytalk",
      build = ":DirtytalkUpdate",
      config = function()
         vim.keymap.set("n", "<F12>", choose_spell_checking)
         vim.keymap.set("n", "z=", function()
            require("telescope.builtin").spell_suggest(require("telescope.themes").get_dropdown())
         end)

         vim.opt.spelllang = { "en_us", "programming" }
         vim.opt.spell = true
      end,
   },
}
