return {
   {
      "nvim-telescope/telescope.nvim",
      dependencies = {
         "nvim-lua/plenary.nvim",
         "nvim-telescope/telescope-dap.nvim",
         "nvim-telescope/telescope-file-browser.nvim",
         "nvim-tree/nvim-web-devicons",
      },
      config = function()
         vim.keymap.set("n", "<leader>fe", "<cmd>Telescope diagnostics severity=Error<cr>")
         vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<cr>")
         vim.keymap.set("n", "<leader>ftb", "<cmd>Telescope dap list_breakpoints<cr>")

         vim.keymap.set("n", "<leader>fb", function()
            require("telescope.builtin").buffers(require("telescope.themes").get_dropdown())
         end)
         vim.keymap.set("n", "<leader>/", function()
            require("telescope.builtin").current_buffer_fuzzy_find({
               sorting_strategy = "ascending",
               layout_config = { prompt_position = "top" },
            })
         end)
         vim.keymap.set("n", "<leader>ff", function()
            local ok = pcall(require("telescope.builtin").git_files, {})
            if not ok then
               require("telescope.builtin").find_files({})
            end
         end)
         vim.keymap.set("n", "<leader>fc", ":Telescope git_status<cr>", { noremap = true })
         vim.keymap.set("n", "<leader><leader>b", function()
            require("telescope").extensions.file_browser.file_browser({
               hidden = is_git_dir(),
            })
         end)
         vim.keymap.set("n", "<leader><leader><leader>b", function()
            require("telescope").extensions.file_browser.file_browser({
               hidden = is_git_dir(),
               path = vim.fn.expand("%:p:h"),
            })
         end)

         function show_file_name_with_path_in_parenthesis(opts, path)
            local tail = require("telescope.utils").path_tail(path)
            local shortened_path = string.gsub(path, "%/" .. tail, "")

            if shortened_path == tail then
               return tail
            end

            return string.format("%s (%s/)", tail, shortened_path)
         end

         function is_git_dir()
            local git_dirs = vim.fs.find(".git", {
               upward = true,
               stop = vim.loop.os_homedir(),
               type = "directory",
            })

            return #git_dirs > 0
         end

         local telescope = require("telescope")
         telescope.setup({
            pickers = {
               find_files = {
                  find_command = { "fd", "-H", "--type", "f", "--strip-cwd-prefix" },
                  path_display = show_file_name_with_path_in_parenthesis,
               },
               git_files = {
                  path_display = show_file_name_with_path_in_parenthesis,
               },
               live_grep = {
                  path_display = show_file_name_with_path_in_parenthesis,
               },
            },
         })
         telescope.load_extension("dap")
         telescope.load_extension("file_browser")
      end,
   },
}
