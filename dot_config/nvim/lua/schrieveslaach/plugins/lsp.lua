local on_attach = function(_, bufnr)
   require("schrieveslaach.lsp.keymaps").lsp_buf_set_keymap(bufnr)
end

return {
   {
      "neovim/nvim-lspconfig",
      dependencies = {
         "williamboman/mason.nvim",
      },
      config = function()
         local lspconfig = require("lspconfig")

         -- TODO: should be move to schrieveslaach.lsp.keymaps
         local opts = { noremap = true, silent = true }
         vim.keymap.set("n", "δ", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
         vim.keymap.set("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)
         vim.keymap.set("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)
         vim.keymap.set({ "n", "v" }, "φ", "<cmd>lua vim.lsp.buf.format({ async = true })<CR>", opts)

         local capabilities = require("cmp_nvim_lsp").default_capabilities()

         lspconfig.ts_ls.setup({
            capabilities = capabilities,
            on_attach = on_attach,

            init_options = {
               plugins = {
                  {
                     name = "@vue/typescript-plugin",
                     location = vim.fn.expand(
                        "$MASON/packages/vue-language-server/node_modules/@vue/language-server/node_modules/@vue/typescript-plugin/"
                     ),
                     languages = { "javascript", "typescript", "vue" },
                  },
               },
            },
            filetypes = {
               "javascript",
               "typescript",
               "vue",
            },
         })
         lspconfig.volar.setup({
            capabilities = capabilities,
            on_attach = on_attach,
            init_options = {
               typescript = {
                  tsdk = vim.fn.expand("$MASON/packages/vue-language-server/node_modules/typescript/lib"),
               },
            },
         })

         lspconfig.jedi_language_server.setup({
            capabilities = capabilities,
            on_attach = on_attach,
         })

         lspconfig.lemminx.setup({
            capabilities = capabilities,
            on_attach = on_attach,
         })

         lspconfig.lua_ls.setup({
            capabilities = capabilities,
            on_attach = on_attach,
            on_init = function(client)
               if client.workspace_folders then
                  local path = client.workspace_folders[1].name
                  if vim.loop.fs_stat(path .. "/.luarc.json") or vim.loop.fs_stat(path .. "/.luarc.jsonc") then
                     return
                  end
               end

               client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
                  runtime = {
                     -- Tell the language server which version of Lua you're using
                     -- (most likely LuaJIT in the case of Neovim)
                     version = "LuaJIT",
                  },
                  -- Make the server aware of Neovim runtime files
                  workspace = {
                     checkThirdParty = false,
                     library = {
                        vim.env.VIMRUNTIME,
                        -- Depending on the usage, you might want to add additional paths here.
                        -- "${3rd}/luv/library"
                        -- "${3rd}/busted/library",
                     },
                     -- or pull in all of 'runtimepath'. NOTE: this is a lot slower and will cause issues when working on your own configuration (see https://github.com/neovim/nvim-lspconfig/issues/3189)
                     -- library = vim.api.nvim_get_runtime_file("", true)
                  },
               })
            end,
            settings = {
               Lua = {},
            },
         })

         lspconfig.clangd.setup({
            cmd = { "clangd", "--compile-commands-dir", "target/cxxbridge/" },
            root_dir = function()
               return "."
            end,
            capabilities = capabilities,
            on_attach = on_attach,
         })

         lspconfig.marksman.setup({
            capabilities = capabilities,
            on_attach = on_attach,
            root_dir = require("lspconfig.util").root_pattern(".git", ".marksman.toml", "SUMMARY.md"),
         })

         lspconfig.nushell.setup({
            cmd = { "nu", "--lsp" },
            cmd_cwd = vim.env.PWD,
            capabilities = capabilities,
            on_attach = on_attach,
         })

         lspconfig.kotlin_language_server.setup({
            cmd_env = {
               JAVA_HOME = require("schrieveslaach.jvm").home(17),
            },
            capabilities = capabilities,
            on_attach = on_attach,
         })
      end,
   },
}
