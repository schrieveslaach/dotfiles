local M = {}

function M.setup()
   vim.api.nvim_create_autocmd("User", {
      pattern = "GitSignsUpdate",
      callback = function(args)
         local ok, gitsigns_status = pcall(function()
            return vim.api.nvim_buf_get_var(args.buf, "gitsigns_status_dict")
         end)

         if not ok then
            return
         end

         local git_url = M.convert_remote_into_gitlab_base_url(gitsigns_status)
         if git_url == nil then
            return
         end

         vim.api.nvim_buf_create_user_command(args.buf, "GitLabOpenLink", function()
            vim.ui.open(M.generate_gitlab_url(args.buf, git_url, gitsigns_status, false))
         end, {})
         vim.api.nvim_buf_create_user_command(args.buf, "GitLabOpenPermaLink", function()
            vim.ui.open(M.generate_gitlab_url(args.buf, git_url, gitsigns_status, true))
         end, {})
         vim.api.nvim_buf_create_user_command(args.buf, "GitLabYankLink", function()
            vim.fn.setreg("+*", M.generate_gitlab_url(args.buf, git_url, gitsigns_status, false))
         end, {})
         vim.api.nvim_buf_create_user_command(args.buf, "GitLabYankPermaLink", function()
            vim.fn.setreg("+*", M.generate_gitlab_url(args.buf, git_url, gitsigns_status, true))
         end, {})
      end,
   })
end

function M.convert_remote_into_gitlab_base_url(gitsigns_status)
   local git_url = nil
   local job_id = vim.fn.jobstart({
      "git",
      "remote",
      "get-url",
      -- TODO: support multiple remotes
      "origin",
   }, {
      cwd = gitsigns_status.root,
      stdout_buffered = true,
      on_stdout = function(_, data)
         local ok, git_url_inner = pcall(function()
            -- TODO: dynamic host
            return string.gsub(string.match(data[1], "gitlab.com.*"), ".git", "")
         end)

         if ok then
            git_url = git_url_inner
         end
      end,
   })

   vim.fn.jobwait({ job_id })

   return git_url
end

function M.generate_gitlab_url(bufnr, git_url, gitsigns_status, permalink)
   local project = require("giturlparser").parse(git_url)

   -- get the current row
   local winid = vim.fn.win_findbuf(bufnr)[1]
   local row = vim.api.nvim_win_get_cursor(winid)[1]

   -- get the file path of the current buffer within the repository
   local path = vim.api.nvim_buf_get_name(bufnr)
   local rel_path = string.sub(path, string.len(gitsigns_status.root) + 2, -1)

   -- get the current commit hash
   if permalink then
      local commit_hash_output = vim.api.nvim_exec2("!git log -1 --format=\\%H", { output = true })
      local commit_hash = vim.trim(
         string.sub(
            commit_hash_output.output,
            string.len(commit_hash_output.output) - 40,
            string.len(commit_hash_output.output)
         )
      )

      return "https://"
         .. project.host
         .. "/"
         .. project.org
         .. "/"
         .. project.repo
         .. "/-/blob/"
         .. commit_hash
         .. "/"
         .. rel_path
         .. "#L"
         .. row
   else
      return "https://"
         .. project.host
         .. "/"
         .. project.org
         .. "/"
         .. project.repo
         .. "/-/blob/"
         .. gitsigns_status.head
         .. "/"
         .. rel_path
         .. "#L"
         .. row
   end
end

return M
