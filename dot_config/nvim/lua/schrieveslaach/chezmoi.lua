local M = {}

function M.apply()
   M.apply_and()
end

function M.apply_and_nvim()
   M.apply_and(function()
      vim.cmd([[ silent exec "!zellij run -fc -n 'testing new chezmoi settings in Neovim' -- nvim" ]])
   end)
end

function M.apply_and_nushell()
   M.apply_and(function()
      vim.cmd([[ silent exec "!zellij run -fc -n 'testing new chezmoi settings in Nushell' -- nu" ]])
   end)
end

function M.apply_and(callback)
   local progress = require("fidget.progress")

   local handle = progress.handle.create({
      title = "applying configuration…",
      lsp_client = { name = "chezmoi" },
   })

   local ok, job_id = pcall(function()
      return vim.fn.jobstart({
         "chezmoi",
         "apply",
      }, {
         on_exit = function(_, data)
            handle:finish()
            if data == 0 then
               if callback then
                  callback()
               end
            end
         end,
      })
   end)

   if not ok then
      vim.notify(job_id, vim.log.levels.ERROR)
      handle:finish()
   end
end

function M.setup()
   vim.api.nvim_create_user_command("ChezmoiApply", M.apply, {})
   vim.api.nvim_create_user_command("ChezmoiApplyAndNvim", M.apply_and_nvim, {})
   vim.api.nvim_create_user_command("ChezmoiApplyAndNushell", M.apply_and_nushell, {})
end

return M
