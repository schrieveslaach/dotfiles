local M = {}

function M.lsp_buf_set_keymap(bufnr)
   vim.keymap.set("n", "gd", require("telescope.builtin").lsp_definitions)
   vim.keymap.set("n", "gi", require("telescope.builtin").lsp_implementations)
   vim.keymap.set("n", "gr", require("telescope.builtin").lsp_references)
   vim.keymap.set("n", "<leader>fd", require("telescope.builtin").diagnostics)

   local opts = { noremap = true, silent = true }
   vim.api.nvim_buf_set_keymap(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
   vim.api.nvim_buf_set_keymap(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
   vim.api.nvim_buf_set_keymap(bufnr, "n", "<C-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
   vim.api.nvim_buf_set_keymap(bufnr, "n", "ρ", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
   vim.api.nvim_buf_set_keymap(bufnr, "n", "α", "<cmd>:lua vim.lsp.buf.code_action()<CR>", opts)
end

return M
