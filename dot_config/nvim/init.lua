vim.opt.syntax = "on"

vim.opt.cursorline = true
vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.smarttab = true
vim.opt.list = true
vim.opt.listchars = "tab:▸\\ ,eol:¬"

vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.breakindent = true
vim.opt.formatoptions = "l"
vim.opt.lbr = true
vim.opt.showbreak = "…"

vim.opt.scrolloff = 5

vim.opt.colorcolumn = "+1"

-- https://stackoverflow.com/a/62097261/5088458
vim.opt.clipboard = "unnamedplus"

vim.opt.completeopt = "menu,menuone,noselect"

-------------------------------------------------------------------------------
-- Custom Commands
-------------------------------------------------------------------------------
require("schrieveslaach.chezmoi").setup()
require("schrieveslaach.gitlab").setup()

-------------------------------------------------------------------------------
-- Mapping Handy Lua functions
-------------------------------------------------------------------------------
require("schrieveslaach.spell").setup()

-------------------------------------------------------------------------------
-- Symbol Theming
-------------------------------------------------------------------------------
vim.cmd([[ sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl= ]])
vim.cmd([[ sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl= ]])
vim.cmd([[ sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl= ]])
vim.cmd([[ sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl= ]])

-------------------------------------------------------------------------------
-- Plugins
-------------------------------------------------------------------------------
require("schrieveslaach.lazy")

-------------------------------------------------------------------------------
-- Configurations That Need to be Applied After Plugins
-------------------------------------------------------------------------------
--Global status bar
vim.opt.laststatus = 3
